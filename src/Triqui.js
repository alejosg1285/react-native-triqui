import React from 'react';
import { Alert, AppRegistry, Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    containerTriqui: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'space-around'
    },
    containerHorizontal: {
        flex: 1,
        flexDirection: 'row'
    },
    containerTable: {
        alignContent: 'center',
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        width: 300
    },
    formInput: {
        borderColor: '#E9967A',
        borderRadius: 10,
        borderWidth: 1,
        fontSize: 22,
        marginBottom: 10,
        padding: 5,
        textAlign: 'center',
        width: 270
    },
    errorLabel: {
        color: 'red',
        fontSize: 15,
        margin: 5
    },
    btnPrimary: {
        textAlign: 'center',
        backgroundColor: '#0000FF',
        color: '#ffffff',
        fontSize: 22,
        padding: 10,
        width: 270
    },
    hideElem: {
        display: 'none'
    },
    titleElem: {
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 15
    },
    subtitleElem: {
        color: '#000080',
        fontSize: 28,
        textAlign: 'center',
    },
    imageThumbnail: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 100,
    },
    squareTable: {
        alignItems: 'center',
        borderColor: '#E9967A',
        borderRadius: 10,
        borderWidth: 1,
        justifyContent: 'center',
        height: 90,
        margin: 5,
        width: 90,
    }
});

export default class Triqui extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            player1: '',
            player2: '',
            errorPlayer1: '',
            errorPlayer2: '',
            showMsgPlayer1: false,
            showMsgPlayer2: false,
            isPlaying: false,
            playingTurno: 'player1',
            imgPlayers: {
                player1: require('./assets/img/icons8-cross.png'),
                player2: require('./assets/img/icons8-round.png')
            },
            posPlayers: {},
            stateButtons: [],
            imageButtons: []
        };

        this.playPosition = this.playPosition.bind(this);
        this.restartGame = this.restartGame.bind(this);
    }

    // Set buttons initial status.
    initGame = () => {
        let states = [];
        let images = [];
        for (let i = 0; i < 9; i++) {
            states.push(false);
            images.push(require('./assets/img/icons8-empty.png'));
        }
        this.setState({ stateButtons: states, imageButtons: images });
    }

    // Check if the players name have been writen and change to true the prop state 'isPlaying'
    startGame = () => {

        let showPlayer1 = (this.state.player1.trim() == "")
        this.setState({ showMsgPlayer1: showPlayer1 });

        let showPlayer2 = (this.state.player2.trim() == "");
        this.setState({ showMsgPlayer2: showPlayer2 });

        this.initGame();

        if (!showPlayer1 && !showPlayer2)
            this.setState({ isPlaying: true });
    }

    // Change the button image and the player name who played the position that recived as parameter.
    playPosition = (pos) => {
        // define quien fue el ultimo jugador
        let played = this.state.playingTurno;
        // ubica la posicion que el jugador toco
        let posPlayed = this.state.posPlayers;
        posPlayed[pos] = played;

        this.setState({ posPlayers: posPlayed });
        // Inhabilita las posiciones jugadas.
        let disabledButtons = this.state.stateButtons;
        disabledButtons[pos] = true;
        this.setState({ stateButtons: disabledButtons });
        // cambia la imagen de la posicion jugada.
        let imageButton = this.state.imageButtons;
        imageButton[pos] = this.state.imgPlayers[played];
        this.setState({ imageButtons: imageButton });

        // define el jugador del proximo turno.
        let nowPlay = (played == 'player1') ? 'player2' : 'player1';
        this.setState({ playingTurno: [nowPlay]});

        //Si se han jugado 5 o mas posiciones.
        var count = Object.keys(posPlayed).length;
        if (count >= 5) this.checkWinner();
    }

    // Check if someone have win the game with last played position.
    checkWinner = () => {
        let posPlayed = this.state.posPlayers;
        let posLines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ];

        let winner = 'not-one';
        if ((posPlayed[0] == posPlayed[1]) && (posPlayed[0] == posPlayed[2])) {
            winner = posPlayed[0];
        } else if ((posPlayed[3] == posPlayed[4]) && (posPlayed[3] == posPlayed[5])) {
            winner = posPlayed[3];
        } else if ((posPlayed[6] == posPlayed[7]) && (posPlayed[6] == posPlayed[8])) {
            winner = posPlayed[6];
        } else if ((posPlayed[0] == posPlayed[3]) && (posPlayed[0] == posPlayed[6])) {
            winner = posPlayed[0];
        } else if ((posPlayed[1] == posPlayed[4]) && (posPlayed[1] == posPlayed[7])) {
            winner = posPlayed[1];
        } else if ((posPlayed[2] == posPlayed[5]) && (posPlayed[2] == posPlayed[8])) {
            winner = posPlayed[1];
        } else if ((posPlayed[0] == posPlayed[4]) && (posPlayed[0] == posPlayed[8])) {
            winner = posPlayed[0];
        } else if ((posPlayed[2] == posPlayed[4]) && (posPlayed[2] == posPlayed[6])) {
            winner = posPlayed[2];
        }

        // Valida si el ultimo jugador gano el juego
        if (winner == 'player1' || winner == 'player2') {
            Alert.alert(
                'Ganador',
                this.state[winner],
                [
                    {text: 'Aceptar', onPress: () => this.restartGame() }
                ]
            );
        } else {
            var count = Object.keys(posPlayed).length;
            // Valida si se han jugado menos de 9 posiciones y reinicia el tablero.
            if (count == 9) {
                Alert.alert(
                    'Ganador',
                    'Ninguno! inicia de nuevo',
                    [
                        { text: 'Aceptar', onPress: () => this.clearTable() }
                    ]
                );
            }
        }
    }

    // Restat the game asking the players name.
    restartGame = () => {
        this.setState({
            player1: '',
            player2: '',
            showMsgPlayer1: false,
            showMsgPlayer2: false,
            isPlaying: false,
            playingTurno: 'player1',
            posPlayers: {},
            stateButtons: [],
            imageButtons: []
        });
    }

    // Clear the positions played and reset the buttons status, preserved the players name.
    clearTable = () => {
        this.setState({
            playingTurno: 'player1',
            posPlayers: {},
            stateButtons: [],
            imageButtons: []
        });

        this.initGame();
    }

    render() {
        if (!this.state.isPlaying) {
            return (
                <View style={styles.container}>
                    <TextInput
                        style={styles.formInput}
                        placeholder="Nombre jugador 1"
                        onChangeText={(userName) => this.setState({ player1: userName })} />
                    <Text style={this.state.showMsgPlayer1 ? styles.errorLabel : styles.hideElem}>Debe ingresar el nombre del jugador 1</Text>
                    <TextInput
                        style={styles.formInput}
                        placeholder="Nombre jugador 2"
                        onChangeText={(userName) => this.setState({ player2: userName })} />
                    <Text style={this.state.showMsgPlayer2 ? styles.errorLabel : styles.hideElem}>Debe ingresar el nombre del jugador 2</Text>
                    <TouchableOpacity onPress={this.startGame}>
                        <Text
                            style={styles.btnPrimary}>
                            Jugar
                        </Text>
                    </TouchableOpacity>
                </View>
            );
        }

        let squares = [];
        for (let i = 0; i < 9; i++) {

            squares.push(
                    <TouchableOpacity 
                        onPress={() => this.playPosition(i)} 
                        style={styles.squareTable} key={i}
                        disabled={this.state.stateButtons[i]}>
                        <Image source={this.state.imageButtons[i]}></Image>
                    </TouchableOpacity>
            );
        }

        return (
            <View style={ styles.containerTriqui }>
                <Text style={styles.titleElem}>Vamos a jugar Triqui</Text>
                <View style={styles.containerTable}>
                    {squares}
                </View>
                <View style={{flexDirection: 'row', margin: 15}}>
                    <View style={{flexDirection: 'column', justifyContent: 'space-around', flexGrow: 1}}>
                        <Text style={{fontSize: 18, color: '#000000'}}>Está jugando:</Text>
                        <Text style={styles.subtitleElem}>{this.state[this.state.playingTurno]}</Text>
                    </View>
                    <TouchableOpacity onPress={() => this.restartGame()}>
                        <Image source={require('./assets/img/icons8-reset.png')}></Image>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

AppRegistry.registerComponent('triquiReactNative', () => Triqui);