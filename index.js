/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import Triqui from './src/Triqui';
import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => Triqui);
